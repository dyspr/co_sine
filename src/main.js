var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var x1 = 0
var y1 = 0
var frameRatio = [0.005 + Math.random() * 0.015, 0.005 + Math.random() * 0.015, 0.005 + Math.random() * 0.015, 0.005 + Math.random() * 0.015, 0.005 + Math.random() * 0.015, 0.005 + Math.random() * 0.015]
var blobHistory = []
var border = 0.3
var blobSize = 0.2
var blink = Math.random()

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < blobHistory.length; i++) {
    if (blink > 0.5) {
      fill(255 * (i / blobHistory.length))
    } else {
      fill(255 - 255 * (i / blobHistory.length))
    }
    noStroke()
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    ellipse(blobHistory[i][0][0] * boardSize, blobHistory[i][0][1] * boardSize, boardSize * blobSize)
    ellipse(blobHistory[i][1][0] * boardSize, blobHistory[i][1][1] * boardSize, boardSize * blobSize)
    pop()
  }

  if (blink > 0.5) {
    fill(255)
  } else {
    fill(0)
  }
  noStroke()
  push()
  translate(windowWidth * 0.5, windowHeight * 0.5)
  ellipse(sin(frameCount * frameRatio[0] * x1 + cos(frameCount * frameRatio[1])) * boardSize * border, cos(frameCount * frameRatio[2] * y1 + sin(frameCount * frameRatio[3])) * boardSize * border, boardSize * blobSize)
  ellipse(cos(frameCount * frameRatio[4]) * boardSize * border, sin(frameCount * frameRatio[5]) * boardSize * border, boardSize * blobSize)
  pop()

  if (blobHistory.length > 256) {
    blobHistory = blobHistory.splice(1)
  }
  blobHistory.push([[sin(frameCount * frameRatio[0] * x1 + cos(frameCount * frameRatio[1])) * border, cos(frameCount * frameRatio[2] * y1 + sin(frameCount * frameRatio[3])) * border], [cos(frameCount * frameRatio[4]) * border, sin(frameCount * frameRatio[5]) * border]])
  blink = Math.random()

  x1 = noise(y1)
  y1 = noise(x1)
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
